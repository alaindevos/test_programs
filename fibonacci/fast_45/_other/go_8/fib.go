package main
import "fmt"

func fib(number int) int {

    if number < 2 { return number }

    return fib(number-1)+fib(number-2)
}

func main() {

    fmt.Printf("Fib: %d\n", fib(45))
} 

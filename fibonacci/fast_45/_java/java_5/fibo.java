public class fibo 
{
    public static void main(String args[]) 
    {
      System.out.println(fib(45));
    }
    public static long fib(long n) 
    {
      if (n < 2) return(n);
      return( fib(n-2) + fib(n-1) );
    }
}

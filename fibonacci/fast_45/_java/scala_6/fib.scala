object fib {
	def fibo(n: Int): Int =
		if (n < 2) n
		else fibo(n - 1) + fibo(n - 2);
	def main(args: Array[String]) {
		val x=args(0).toInt;
		Console.println("fib("+ x +") = " + fibo(x));
    }
}

class Point(xc: Int, yc: Int) 
{
  var x: Int = xc
  var y: Int = yc
  def move(dx: Int, dy: Int) { x = x + dx  ; y = y + dy }
  def +(p: Point) =new Point(x+p.x,y+p.y)
  def norm = math.sqrt(x*x+y*y) 
  override def toString(): String  = "(" + x + ", " + y + ")";
}

object testcomplex 
{
    def main(args: Array[String]) 
    {
      var p1 = new Point(1,0)
      var p2 = new Point(2,2)
      var p3 = new Point(0,0)
      p1.move(0,1)
      p3 = p1 + p2
      p3 = p3.+(p1)
      println(p3)
      println(p3.norm)
    }
}

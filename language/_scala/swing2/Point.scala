class Point(xc: Int, yc: Int) 
{
  var x: Int = xc
  var y: Int = yc
  def +(p: Point) =new Point(x+p.x,y+p.y)
  override def toString(): String  = "(" + x + ", " + y + ")";
}

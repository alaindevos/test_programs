import scala.swing._
import scala.swing.event._

object Myui extends SimpleGUIApplication 
	{
	override def main(args: Array[String]) = super.main(args)
	def top = new MainFrame 
		{
		title = "Second Swing App"
		val button = new Button  {text = "Add complex"}
		val t1_x= new TextField {text=""}
		val t1_y= new TextField {text=""}
		val t2_x= new TextField {text=""}
		val t2_y= new TextField {text=""}
		val tr_x= new TextField {text="Result"}
		val tr_y= new TextField {text="Result"}
		
		contents = new BoxPanel(Orientation.Vertical) 
			{
			contents+= t1_x;
			contents+= t1_y;
			contents+= t2_x;
			contents+= t2_y;
			contents+= button
			contents+= tr_x;
			contents+= tr_y;
			border = Swing.EmptyBorder(30, 30, 10, 30)
			}
			
		 listenTo(button)
		 reactions += 
			{
			case ButtonClicked(b) =>
				var p1 = new Point(t1_x.text.toInt,t1_y.text.toInt)
				var p2 = new Point(t2_x.text.toInt,t2_y.text.toInt)
				var p3 = p1+p2
				tr_x.text=p3.x.toString()
				tr_y.text=p3.y.toString()
			}
 
		}
	}


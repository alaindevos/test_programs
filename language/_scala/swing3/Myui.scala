import scala.swing._
import scala.swing.event._

object Myui extends SimpleGUIApplication 
	{
	override def main(args: Array[String]) = super.main(args)

	def settextfield(tf1:TextField,txt1:String):Unit={tf1.text=txt1}
	def settextfield(tf1:TextField,i1:Int):Unit={settextfield(tf1,i1.toString)}
    def settextfield(tf1:TextField,i1:Int,tf2:TextField,i2:Int):Unit=
	{settextfield(tf1,i1);settextfield(tf2,i2)}

	def top = new MainFrame 
		{
		title = "Second Swing App"
		val button = new Button  {text = "Add complex"}
		val t1_x= new TextField {text=""}
		val t1_y= new TextField {text=""}
		val t2_x= new TextField {text=""}
		val t2_y= new TextField {text=""}
		val tr_x= new TextField {text="Result"}
		val tr_y= new TextField {text="Result"}

		contents = new BoxPanel(Orientation.Vertical) 
			{
			 for ( x<- List(t1_x,t1_y,t2_x,t2_y,button,tr_x,tr_y))
				contents+=x
			 border = Swing.EmptyBorder(30, 30, 10, 30)
			}
		
		 listenTo(button)
		 reactions += 
			{
			case ButtonClicked(b) =>
				var p1 = new Point(t1_x.text,t1_y.text)
				var p2 = new Point(t2_x.text,t2_y.text)
				var p3 = p1+p2
				settextfield(tr_x,p3.x,tr_y,p3.y)
			}
 
		}
	}


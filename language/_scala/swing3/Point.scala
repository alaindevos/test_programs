class Point(xc: Int, yc: Int) 
{
  def this(xs:String,ys:String){this(xs.toInt,ys.toInt);}
  var x: Int = xc
  var y: Int = yc
  def +(p: Point) =new Point(x+p.x,y+p.y)
  override def toString(): String  = "(" + x + ", " + y + ")";
}

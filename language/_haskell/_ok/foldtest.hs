import Debug.Trace

--myprint:: [Int] -> IO()
--myprint a = putStrLn(show a)


myappend :: [Int] -> [Int] -> [Int]
myappend xs ys = trace ("xs"++show(xs)++"  ys:"++show(ys)) foldr (:) ys xs


main = do
		let a= [1,2,3]
		    b= [3,4,5]
		    c= myappend a b
		    in print (show (c))
 
